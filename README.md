
**Installation guide**


## Installation:


1. Clone repository into your local server's root directory
2. Rename the root folder from "GLAD_image_uploader" into "glad"
3. In your localhost connection, create a database called 'glad'
4. Import glad.sql into the database
5. Change the database settings under modules/database/config/database.php if necessary
