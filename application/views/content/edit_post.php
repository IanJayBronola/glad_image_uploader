<hr>
<input type="hidden" name="img_id" value="<?= $image->img_id ?>">
<div class="row">
    <div class="col-2">Title</div>
    <div class="col-4"><input type="text" name="title" required value="<?= $image->img_title ?>"></div>
</div>
<div class="row">
    <div class="col-2">File Name</div>
    <div class="col-4"><input type="text" required name="filename" value="<?= $image->img_file_name ?>"></div>
</div>
<div class="row">
    <div class="col-2">File</div>
    <div class="col-4"><input type="file" name="image_file"></div>
</div>
<div class="row">
    <div class="col-4">
        <img width="80%" src="uploads/images/<?= date('Y-m-d', strtotime($image->img_date_created)) ?>_<?= $image->img_id ?>/<?= $image->img_file_name ?>.<?= $image->img_ext ?>" alt="">
    </div>
</div>
<hr>
<div class="row">
    <div class="col-4">
        <button class="btn success" type="submit">Update</button>
        <button class="btn error close-modal">Cancel</button>
    </div>
</div>