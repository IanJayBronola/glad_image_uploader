<?php 
    foreach($images as $image){
?>
        <tr>
            <td><?= $image->img_title ?></td>
            <td><img src="<?= URL::base() ?>uploads/images/<?= date('Y-m-d', strtotime($image->img_date_created)) ?>_<?= $image->img_id ?>/thumb_<?= $image->img_id ?>.jpg" alt=""></td>
            <td><?= $image->img_file_name ?></td>
            <td><?= date('M d, Y', strtotime($image->img_date_created)) ?></td>
            <td>
                <a href="<?= URL::base() ."homepage/view_edit/{$image->img_id}" ?>" onclick="open_modal(this,'post-editor'); show_edit(this)" class="btn success small">Edit</a>
                <a href="#" onclick="open_modal(this,'delete-post')" img-id="<?= $image->img_id ?>" delete-post-btn class="btn error small">Delete</a>
            </td>
        </tr>
       
    <?php
    }
?>
<tr>
    <td colspan="4">
        <ul class="pagination">
        <?php if(ceil($total_images / 10) > 1): ?>
            <li><a href="<?= URL::base() ."homepage/show_table/1" ?>"> << </a></li>
        <?php endif ?>
        <?php
            for($i = 1; $i <= ceil($total_images / 10); $i ++ ){
                if($current_page == $i){
                    echo "<li><a href='#'  class='active' disabled='disabled' >{$i}</a></li>";
                }
                else{
                    echo "<li><a href='". URL::base() ."homepage/show_table/{$i}'>{$i}</a></li>";
                }
            }
        ?>
        <?php if(ceil($total_images / 10) > 1): ?>
            <li><a href="<?= URL::base() ."homepage/show_table/".ceil($total_images / 10)  ?>"> >> </a></li>
        <?php endif ?>
        </ul>
    </td>
</tr>