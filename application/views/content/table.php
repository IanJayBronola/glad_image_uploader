<div class="row">
    <div class="full">
        <h4 class="sub-title">
            Upload File:
        </h4>
    </div>
    <form action="<?= url::base() ?>homepage/upload/<?= $current_page ?>" method="post" id="upload-form" enctype="multipart/form-data">
        <div class="input-group col-3">
            <label for="">Title</label>
            <input type="text" required name="title">
        </div>
        <div class="input-group col-4">
            <label for="">Filename</label>
            <input type="text" required name="filename">
        </div>
        <div class="input-group col-3">
            <label for="file_input" required> File </label>
            <input type="File" id='file_input' name="image_file" >
        </div>
        <div class="col-2">
        <button class="btn">Upload</button>
        </div>
    </form>
</div>
<div class="panel">
    <div class="responsive"> 
    <table class="table">
        <thead>
            <th>Title</th>
            <th width="100px">Thumbnail</th>
            <th>Filename</th>
            <th>Date Added</th>
            <th></th>
        </thead>
        <tbody id='tbl-bdy'>
           <?php
                echo $tbl_body
           ?>
        </tbody>
    </table>
    </div>
</div>