<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= HTML::style('assets/css/main.css'); ?>
   <?= HTML::script('assets/js/jQuery.min.js'); ?>
   <?= HTML::script('assets/js/sizzle.js'); ?>
   <?= HTML::script('assets/js/fileUploader.js'); ?>
</head>
<body>
    
    <div class="title">
        <h2><?= $title ?></h2>
        <small class="sub-title">
            <?= $sub_title ?>
        </small>
    </div>
    <div class="content">
        <?= $content ?>
    </div>
    <div class="footer">
        <p>All rights reserved &copy; <?= date('Y') ?></p>
    </div>
    <div class="modal" id="post-editor">
        <div class="container">
            <h3 class="sub-title">Edit Post</h3>
            <form action="<?= url::base() ?>homepage/upload" method="post" id="edit-view" enctype="multipart/form-data">
                <i>Loading Information . . .</i>
            </form>
        </div>
    </div>
    <div class="modal" id="delete-post">
        <div class="container">
            <h3 class="sub-title">Delete Post?</h3>
            <a class="btn small" sure-delete-btn href="<?= URL::base() ."homepage/delete_image" ?>" show-url="<?= URL::base() ."homepage/show_table" ?>"> Yes</a>
            <a class="btn error close-modal small">No</a>
        </div>
    </div>
</body>
</html>