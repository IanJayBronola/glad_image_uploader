<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Homepage extends Controller_Template {

    public $template = 'template/fileuploader';

    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
    }
    
	public function action_index()
	{
        $images = ORM::factory('image')->offset(0)->limit(10)->order_by('img_id', 'desc')->find_all();
        $total_images = ORM::factory('image')->find_all()->count();
        $this->template->title = 'G.L.A.D.';
        $this->template->sub_title = 'File Uploader';
        $this->template->content = View::factory('content/table')
                                    ->set('current_page',1)
                                    ->set('tbl_body', View::factory('content/table_body')->bind('images',$images)->bind('total_images', $total_images)->set('current_page',1));
    }
    public function action_upload()
    {   
        $ret = ['success' => true];
        $total_images = ORM::factory('image')->find_all()->count();
        $page = $this->request->param('page');
        $offset = !isset($page)  ? 0 : 1 + (10 * ($page - 1));
        $file = Validation::factory($_FILES);

        if($this->request->post('img_id')){
            $rules=  array(
                array(array('Upload', 'valid')),
                ['Upload::size', array(':value', '100M')],
                array('Upload::type', array(':value', array('jpg', 'png', 'gif')))
            );
        }else{
            $rules =  array(
                array(array('Upload', 'valid')),
                ['Upload::size', array(':value', '100M')],
                array(array('Upload', 'not_empty')),
                array('Upload::type', array(':value', array('jpg', 'png', 'gif')))
            );
        }
        
        $file->rules(
            'image_file',
            $rules
          );     
          $checked = $file->check(); 
          if ($checked){    

            $filename = explode('.', $_FILES['image_file']['name']);

            $file = Validation::factory($_FILES);
            if($this->request->post('img_id')){
                $image = ORM::factory('image', $this->request->post('img_id'));
            }
            else{
                $image = ORM::factory('image');
                $image->img_date_created = date('Y-m-d');
            }
            $image->img_title = $this->request->post('title');
            $image->img_file_name = $this->request->post('filename');
            $image->img_ext = isset($filename[1]) ? strtolower($filename[1]): $image->img_ext;
            if($this->request->post('img_id')){

            $image->update();
            }
            else{
            $image->save();
                
            }
                if(isset($filename[1])){
                    $directory = DOCROOT.'uploads/images/'.date('Y-m-d', strtotime($image->img_date_created))."_".$image->pk();
                    if(!is_dir($directory)){
                        mkdir($directory,0777,TRUE);
                    }
                    
                    $filepath = Upload::save($_FILES['image_file'],  $image->pk().'.'. strtolower($image->img_ext), $directory);  

                    $thumb_path = $directory.'/thumb_'.$image->pk().'.jpg';
                    Image::factory($filepath)->resize(100, NULL)->save($thumb_path);
                }

            $view = new View('content/table_body');
            
            $images = ORM::factory('image')->offset(0)->limit(10)->order_by('img_id', 'desc')->find_all();
            $view->images = $images;
            $view->total_images = $total_images;
            $view->current_page = $page;
            $ret['view'] =  $view->render();
            
          }
          else{
              $ret['success'] = false;
              $ret['view'] =  $file->errors('errors')['image_file'] == "errors.image_file.Upload::size" ? "File too big." : $file->errors('errors')['image_file'];
           
          }

          echo json_encode($ret);
        
    }
    public function action_show_table()
    {
        $page = $this->request->param('page') ? $this->request->param('page')  : 1;
        $offset = $this->request->param('page') ?  1 + (10 * ($page - 1)) : 0;
        $images = ORM::factory('image')->offset($offset)->limit(10)->order_by('img_id', 'desc')->find_all();
        $total_images = ORM::factory('image')->find_all()->count();
        echo View::factory('content/table_body')->bind('images',$images)->bind('total_images', $total_images) ->set('current_page',$page);
    }
    public function action_view_edit()
    {
        $id = $this->request->param('page');
        $image = ORM::factory('image')->find($id);
        echo View::factory('content/edit_post')->bind('image',$image);
    }
    public function action_delete_image()
    {
        $id = $this->request->param('page');
        $image = ORM::factory('image')->find($id)->delete();
    }
    public function before()
    {
            if (in_array($this->request->action(), array('upload','show_table', 'view_edit','delete_image')))
            {
                    $this->auto_render = FALSE;
            }

            parent::before();
    }
    

} // End Homepage
