var img_id;
$(function(){
    
    // $('#upload-form').submit(function(e){
  
    // })
})
$(document).on('submit','#upload-form, #edit-view',function(e){
        
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        var data = new FormData(this);
        form.find('button').attr('disabled','disabled');
        $('#tbl-bdy').css({opacity:'0.5'});
        $.ajax({
               type: "POST",
               enctype: 'multipart/form-data',
               url: url,
               data: data,
               dataType:'JSON',
               processData: false,
                contentType: false,
               success: function(data)
               {
                   if(data.success == true){
                    $('#tbl-bdy').html(data.view);
                    addNotif("Upload Successful!","success","");
                    $('img').each(function(k,v){
                        $(v).attr("src", $(v).attr("src")+"?timestamp=" + new Date().getTime())
                    })
                    
                    form.find('input').val("");
                    $('.close-modal').trigger('click');
                }
                   else{
                    addNotif("Upload error!","error",data.view);
                   }
                   $('#tbl-bdy').css({opacity:'1'});
                   form.find('button').removeAttr('disabled');
                }
             });
})
$(document).on('click','.pagination li a', function(e){
            var t = $(this);
            e.preventDefault();
        if(t.hasClass('active') == false){
            $('#tbl-bdy').css({opacity:'0.5'});

            $.ajax({
                url: t.attr('href'),
                success: function(data)
                {
                    $('#tbl-bdy').html(data);
                    $('#tbl-bdy').css({opacity:'1'});
                }
            });
        }
})
$(document).on('click','.close-modal', function(e){
    e.preventDefault();
    $('.modal').fadeOut();
})
$(document).on('change','#post-editor input[type=file]', function(){
    readURL(this);
})
$(document).on('click','[delete-post-btn]', function(){
  img_id = $(this).attr('img-id');
    
})
$(document).on('click','[sure-delete-btn]', function(e){
    e.preventDefault();
    var t = $(this);
    $.ajax({
    url: t.attr('href')+"/"+img_id,
    success: function(data)
    {
        addNotif('Delete Post Successful',"success","");
        $('.close-modal').trigger('click');
        refresh_view(t.attr('show-url'));
    }
    });
})

var readURL = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#post-editor img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
var addNotif = function(title, bg, msg){
    var elem = $('<div class="notification '+bg+'"> \
                    <div class="notif-title">'+title+'</div> \
                    <div class="notif-content">'+msg+'</div> \
                </div>');
    $('body').prepend(elem);
    setTimeout(() => {
        elem.fadeOut();
    }, 2000);
}
var open_modal = function(e,id){
    // e.preventDefault();
    $('.modal#'+id).fadeIn();
    return false;
}
var show_edit = function(e,id){

    var t = $(e);
    window.event.preventDefault();
    $.ajax({
        url: t.attr('href'),
        type: 'POST',
        success: function(data)
        {
            $('#edit-view').html(data);
         }
      });
}
var refresh_view = function(url){
    $('#tbl-bdy').css({opacity:'0.5'});
    $.ajax({
        url: url,
        success: function(data)
        {
             $('#tbl-bdy').html(data);
            $('#tbl-bdy').css({opacity:'1'});
         }
      });
}